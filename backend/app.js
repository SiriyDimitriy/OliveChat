/**
 *  Dev dependencies
 */
var sessionConfig = require('./app/config/dev/session/session');
var SwaggerExpress = require('swagger-express-mw');
var session = require('express-session');
var passport = require('passport');
var mongoose = require('mongoose');
var passportGithub = require('./app/bootstrap/auth/authGITHUB');
const socket = require('./api/routes/socket');
/**
 *  App itself
 * */
var app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

socket(io);
// Headers config
// app.use(head.headerConfig);

var config = {
  appRoot: __dirname // required config
};

/**
 *  Router for public request
 */
var PublicRouter = require('express').Router();

/**
 *  Mongoose connection
 */
mongoose.connect('mongodb://localhost/olivka-social-auth');

/**
 *  Fake login page
 */
PublicRouter.get('/login',(req,resp)=>{
    resp.send('Login page');
});

/**
 *  Logout page
 */
PublicRouter.get('/logout',(req,resp)=>{
    req.logout();
    resp.send('Logged out!');
});

/**
 *  Passport.js GitHub verification
 */
PublicRouter.get('/auth/github', passportGithub.authenticate('github', { scope: [ 'user:email' ] }));
PublicRouter.get('/auth/github/callback',
    passportGithub.authenticate('github',
        { failureRedirect: '/login' }),(req, res)=> {res.json(req.user);
  });

app.use(session(sessionConfig));
app.use(passport.initialize());
app.use(passport.session());
app.use(PublicRouter);

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, 
    function(err, swaggerExpress) {
      var port = process.env.PORT || 10010;
      if (err) {
        throw err;
      }
      swaggerExpress.register(app);
      app.listen(port);
});

module.exports = app;