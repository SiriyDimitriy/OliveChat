let passport = require('passport');
let GitHubStrategy = require('passport-github').Strategy;

let User = require('../../models/modelUser');
let config = require('../../config/dev/social/github');
let modelSerializer = require('../serializer/serializer');


passport.use(new GitHubStrategy({
        clientID: config.clientID,
        clientSecret: config.clientSecret,
        callbackURL: config.callbackURL
    },
    function(accessToken, refreshToken, profile, done) {

        let searchQuery = {name: profile.displayName};

        let updates = {
            name: profile.displayName,
            someID: profile.id
        };

        let options = {upsert: true};

        User.findOneAndUpdate(searchQuery, updates, options,
            function(err, user) {
                if(err) {
                    return done(err);
            } else {
                return done(null, user);
            }
        });
    }
));

modelSerializer();
module.exports = passport;
