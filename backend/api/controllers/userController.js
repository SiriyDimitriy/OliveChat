'use strict';

module.exports = {
  getAllUsers: getAllUsers,
  getUsersById: getUsersById,
  storeUser: storeUser,
  updateUser: updateUser,
  deleteUser: deleteUser,
  getUserMetaData: getUserMetaData
}

var UserList = [
    {
        id: 1,
        name: 'Vlad',
        email: 'Vlad email',
        password: 'Vlad password',
        metadata: {
            age: 21
        }
    },
    {
        id: 2,
        name: 'new User',
        email: 'new User email',
        password: 'new User password',
        metadata: {
            age: 42
        }
    }
];

function getAllUsers (req, res, next) {
    res.json(UserList);
}

function getUsersById (req, res, next) {
    var userId = req.swagger.params.id.value;
    UserList.forEach(item => {
        if(item.id == userId) {
            res.json(item);
        }
    })
}

function storeUser (req, res, next) {
    var userData = req.swagger.params.userData.value;
    UserList.push(userData);
    res.json(userData);
}

function updateUser (req, res, next) {
    var userId = req.swagger.params.id.value;
    var userData = req.swagger.params.userData.value;
    UserList.forEach(item => {
        if(item.id == userId) {
            Object.assign(item, userData);
            res.json(item);
        }
    })
}

function deleteUser (req, res, next) {
    var userId = req.swagger.params.id.value;
    UserList.forEach(item => {
        if(item.id == userId) {
            var index = UserList.indexOf(item);
            UserList.splice(index, 1);
            res.json(item);
        }
    })
}

function getUserMetaData (req, res, next) {
    var userId = req.swagger.params.id.value;
    UserList.forEach(item => {
        if(item.id == userId) {
            res.json(item);
        }
    }) 
}
