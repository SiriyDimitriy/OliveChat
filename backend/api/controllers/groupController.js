var util = require('util');
var request = require('request');

var data = [
		{
		id: 0,
		name: 'Friiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiends',
		members: [
			'Homeless',
			'Grisha',
			'Fedya',
			'Anastasia',
			'Frank',
			'Viktor',
			'Yosif',
			'Illich',
			'Adolf',
			'Mick Jagger',
			'Ronaldinho',
			'Ahmed',
			'Buddah',
			'Abraham',
			'Dzi Dzo',
			'Magistr Yoda',
			'Dart Weider',
			'Benito',
			'Che',
			'Zaha',
			'Frank Sinatra',
			]
		},
		{
		id: 1,
		name: 'Brothers',
		members: ['User21', 'User22', 'User23']
		},
		{
		id: 2,
		name: 'Sisters',
		members: ['User31', 'User32', 'User33']
		},
		{
		id: 3,
		name: 'Neighborhoods',
		members: ['User41', 'User42', 'User43']
		},
		{
		id: 4,
		name: 'Classmates',
		members: ['User11', 'User12', 'User13']
		},
		{
		id: 5,
		name: 'Homeless',
		members: ['User61', 'User62', 'User63']
		},
		{
		id: 6,
		name: 'Strangers',
		members: ['User61', 'User62', 'User63']
		},
		{
		id: 6,
		name: 'Gods',
		members: ['User61', 'User62', 'User63']
		},
		{
		id: 6,
		name: 'Devils',
		members: ['User61', 'User62', 'User63']
		},
		{
		id: 6,
		name: 'Femen',
		members: ['User61', 'User62', 'User63']
		},
];

module.exports = {
	getAllGroups: getAllGroups,
	getGroupById: getGroupById,
	createGroup: createGroup,
	updateGroupById: updateGroupById,
	deleteGroupById: deleteGroupById
};

function getAllGroups (request, response, next) {
	response.json(data);
}

function getGroupById (request, response, next) {
	var userId = request.swagger.params.id.value;
	response.json(data.find(item => item.id === userId));
}

function createGroup (request, response, next) {
	var newGroup = request.swagger.params.body.value;
	data.push(JSON.stringify(newGroup));
	response.json(newGroup);
}

function updateGroupById (request, response, next) {
	var groupId = request.swagger.params.id.value;
	var newGroup = request.swagger.params.body.value;

	data.forEach(item => {
		if (item.id === groupId) {
			Object.assign(item, newGroup);
			response.json(item);
		}
	})
}

function deleteGroupById (request, response, next) {
	var groupId = request.swagger.params.id.value;
	var indexSearchedEl;
	data.forEach(item => {
		if (item.id === groupId) {
			indexSearchedEl = data.indexOf(item);
			data.splice(indexSearchedEl,1);
			response.json(item);
		}
	});
}