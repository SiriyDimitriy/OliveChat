var data = [
    {
        id: 0,
        name: 'Group1',
        members: ['User11', 'User12', 'User13']
    },
    {
        id: 1,
        name: 'Group2',
        members: ['User21', 'User22', 'User23']
    },
    {
        id: 2,
        name: 'Group3',
        members: ['User31', 'User32', 'User33']
    },
    {
        id: 3,
        name: 'Group4',
        members: ['User41', 'User42', 'User43']
    },
    {
        id: 4,
        name: 'Group5',
        members: ['User51', 'User52', 'User53']
    },
    {
        id: 5,
        name: 'Group6',
        members: ['User61', 'User62', 'User63']
    }
];

module.exports = {
    getGroupMembersById: getGroupMembersById
};

function getGroupMembersById (request, response) {
    var groupId = request.swagger.params.id.value;
    response.json(data.find(item => item.id = groupId).members);
}

