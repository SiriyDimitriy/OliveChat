'use strict';

module.exports = {
  getAllGroupMessages: getAllGroupMessages,
  getGroupMessage: getGroupMessage
}

var groupList = [
    {
        id: 1,
        name: "Great dialog",
    },
    {
        id: 2,
        name: "Bad dialog",
    }
];

var messageList = [
    {
        id: 1,
        groupId: 1,
        author: "Vlad",
        text: "Hello Vlad",
    },
    {
        id: 2,
        groupId: 1,
        author: "Artur",
        text: "Hello Artur",
    },
    {
        id: 3,
        groupId: 2,
        author: "Angi",
        text: "Hello Angi",
    },
    {
        id: 4,
        groupId: 2,
        author: "Yoda",
        text: "Hello Yoda",
    },
];

var responseData = {};
var responseMessages = [];

function getAllGroupMessages(req, res, next) {
    var reqGroupId = req.swagger.params.groupId.value;
    groupList.forEach(item => {
        if(item.id == reqGroupId) {
            responseData.groupId = item.id;
            responseData.name = item.name;
        }
    })
    messageList.forEach(item => {
        if(item.groupId == reqGroupId) {
            responseMessages.push(item);
        }
    })
    responseData.messages = responseMessages;
    res.json(responseData);
}

function getGroupMessage(req, res, next) {
    var reqGroupId = req.swagger.params.groupId.value;
    var reqMessageId = req.swagger.params.messageId.value;
    groupList.forEach(item => {
        if(item.id == reqGroupId) {
            responseData.groupId = item.id;
            responseData.name = item.name;
        }
    })
    messageList.forEach(item => {
        if(item.id == reqMessageId) {
            responseMessages.push(item);
        }
    })
    responseData.messages = responseMessages;
    res.json(responseData);
}