'use strict';

var util = require('util');
var request = require('request');

var messagesList = [
    {
        id: 1,
        author: "Vlad",
        messageBody: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali"
    },
    {
        id: 2,
        author: "Vlad",
        messageBody: "Hi, everyone"
    },
    {
        id: 3,
        author: "Vlad",
        messageBody: "Hello"
    },
    {
        id: 4,
        author: "Vlad",
        messageBody: "What's up?"
    },
    {
        id: 5,
        author: "Bogdan",
        messageBody: "Great! Watched yesterday match?"
    },
    {
        id: 6,
        author: "Natali",
        messageBody: "Your football, again..."
    },
    {
        id: 7,
        author: "Vlad",
        messageBody: "Mesut Ozil was amazing... such dribling and passing skills!"
    },
    {
        id: 8,
        author: "Natali",
        messageBody: "Stop!!! Please"
    },
    {
        id: 9,
        author: "Bogdan",
        messageBody: "Chealsea played awfull, why Hazard was sitting on bench?"
    },
    {
        id: 10,
        author: "Vlad",
        messageBody: "Heard that Messi is coming to Arsenal?"
    },
    {
        id: 11,
        author: "Bogdan",
        messageBody: "great...((( What Barca will do without him?"
    },
    {
        id: 12,
        author: "Natali",
        messageBody: "It realy annoying, bye)"
    },
    {
        id: 13,
        author: "Vlad",
        messageBody: "Bye"
    },
    {
        id: 14,
        author: "Bogdan",
        messageBody: "see you in university"
    },
    {
        id: 15,
        author: "Bogdan",
        messageBody: "I ll be there at 14 00"
    },
    {
        id: 16,
        author: "Bogdan",
        messageBody: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ali"        
    },
]

module.exports = {
    getAllMessages: getAllMessages,
    createMessage: createMessage,
    getMessageById: getMessageById,
    updateMessage: updateMessage,
    deleteMessage: deleteMessage
}

function getAllMessages (req, res, next) {
    res.json(messagesList)
}

function getMessageById (req, res, next) {
    var messageId = req.swagger.params.id.value;
    res.json(messagesList.find(item => item.id === messageId));
}

function createMessage (req, res, next) {
    var userMessage = req.swagger.params.userMessage.value;
    messagesList.push(userMessage);
    res.json(userMessage);
}

function updateMessage (req, res, next) {
    var messageId = req.swagger.params.id.value;
    var userMessage = req.swagger.params.userMessage.value;
    messagesList.forEach(item => {
        if(item.id == messageId) {
            Object.assign(item, userMessage);
            res.json(item);
        }
    })
}

function deleteMessage (req, res, next) {
    var messageId = req.swagger.params.id.value;
    messagesList.forEach(item => {
        if(item.id == messageId) {
            var index = messagesList.indexOf(item);
            messagesList.splice(index, 1);
            res.json(item);
        }
    })
}