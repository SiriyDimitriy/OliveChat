const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var schemaChanel = new Schema({
    author: {
        type: Schema.Types.ObjectId,
      //  required: true,
        ref:'User'
    },
    chanelUsers:[
        {
            type: Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    chanelMessages:[
        {
            type: Schema.Types.ObjectId,
            ref: 'Message'
        }
    ],
    meta:{
        chanelName:{
           type:String,
            required:true
        },
        maxUsers:{
            type:Number,
            default:2
        },
        description:{
            type:String,
        },
        chanelTag:{
            type:String,
            required:true,
            unique: true
        },
        created_at:{
            type: Date,
            default: Date.now
        },
        updated_at:{
            type: Date,
            default: Date.now
        }
    }
});

module.exports = schemaChanel;