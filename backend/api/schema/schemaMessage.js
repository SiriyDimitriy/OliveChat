const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var schemaMessage = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        required: true,
        ref:'User'
    },
    meta: {
        created_at: {
            type: Date,
            default: Date.now
        }
    },
    text:{
        type:String,
        required:true,
        minlength:2
    },
    link:{
        type: Schema.Types.ObjectId,
        required: true
    }
});

module.exports = schemaMessage;