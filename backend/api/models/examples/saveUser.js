const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.connect(`mongodb://localhost:27017/testOliveDB`);

const chanelModel = require("../modelChanel");
const userModel = require("../modelUser");
const userACL = require("../modelACL");
const hashModel = require("../modelHash");
const settingsModel =require("../modelSettings");
const chanelGeneral = new chanelModel({
    meta:{
        chanelName:"First chanel",
        maxUsers:12,
        chanelTag:"@lastChanel"
    }
});
chanelGeneral.save((err,chanel)=>{
    err?console.log("Chanel Error:::",err):console.log("Saved chanel with id:: ",chanel._id);
    let hash = new hashModel({
        hashVal:"password"
    });
    hash.save((err,hash)=>{
        err?console.log("Hash Error:::",err):console.log("Saved hash with id:: ",hash._id);
        var settings=new settingsModel({});
        settings.save((err,settings)=>{
            err?console.log("Settings Error:::",err):console.log("Saved settings with id:: ",settings._id);
            var user =  new userModel({
                name:"Igor",
                email:"mfc2005@ukr.net",
                meta:{
                    age:34
                },
                hash:hash,
                settings:settings
            });
            user.save((err,user)=>{
                err?console.log("User Error:::",err):console.log("Saved User with id:: ",user._id);
                var igorACL = new userACL({
                    permissions:[{
                        entity:chanel,
                        actions:["read","write"]
                    }]
                });
                igorACL.save((err,acl)=>{
                    err?console.log("ACL Error:::",err):console.log("Saved acl with id:: ",acl._id);
                    user.update({permissions:igorACL},(err,raw)=>{
                        err?console.log("User Error:::",err):console.log("Saved with data:: ",raw);
                    });
                });
            });
        });
    });
});