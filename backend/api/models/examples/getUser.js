const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.connect(`mongodb://localhost:27017/testOliveDB`);

const chanelModel = require("../modelChanel");
const userModel = require("../modelUser");
const userACL = require("../modelACL");
const hashModel = require("../modelHash");
const settingsModel =require("../modelSettings");
const util = require("util");
const log = console.log;

userModel.find()
    .populate("settings hash permissions")
    .exec()
    .then(function(data){
         //log(util.inspect(data[0].permissions[0].permissions[0].actions,5,4));
        log(data);
        mongoose.disconnect();
    });