var mongoose = require('mongoose');
mongoose.connect(`mongodb://localhost:27017/storyTest`);
var db = mongoose.connection;
var Schema = mongoose.Schema;

var personSchema = Schema({
    _id     : Number,
    name    : String,
    age     : Number,
    stories : [{ type: Schema.Types.ObjectId, ref: 'Story' }]
});

var storySchema = Schema({
    _creator : {
        type: Number,
        ref: 'Author'
    },
    title    : String,
    fans     : [{ type: Number, ref: 'Person' }]
});

var Story  = mongoose.model('Story', storySchema);
var Author = mongoose.model('Author', personSchema);

var aaron = new Author({ _id: 12, name: 'Aaron', age: 100 });

aaron.save(function (err) {
    if (err) console.log("Error:::::::::::",err);
    console.log("Aoron id: ",aaron._id);

    var story1 = new Story({
        title: "Once upon a timex.",
        _creator: aaron._id
    });

    story1.save(function (err) {
        if (err) console.log("Error:::::::::::",err);
        Story
            .findOne({ title: 'Once upon a timex.' })
            .populate('_creator')
            .exec(function (err, story) {
                if (err) return console.log(err);
                console.log('The creator is %s', story._creator.name);
                // prints "The creator is Aaron"
                mongoose.connection.close();
            });


    });
});





