var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var personSchema = Schema({
    _id     : Number,
    name    : String,
    age     : Number,
    stories : [{ type: Schema.Types.ObjectId, ref: 'Story' }]
});

var storySchema = Schema({
    _creator : {
        type: Number,
        ref: 'Author'
    },
    title    : String,
    fans     : [{ type: Number, ref: 'Person' }]
});