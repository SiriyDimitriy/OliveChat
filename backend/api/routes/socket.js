module.exports = function (io) {
	io.sockets.on('connect', (socket) => {
		socket.on('send:message', data => {
			socket.broadcast.emit('send:message', data);
		});
	});

	io.listen(2000);
}