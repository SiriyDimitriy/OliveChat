const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const restify = require('express-restify-mongoose');

// Middleware
const head = require('./back/midleware/headers.config');

// Models
// TODO provide schema when it will be raedy
const userModel = require('./back/models/user.model');

const app = express();
const router = express.Router();

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/users');

// Body parser config
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Headers config
app.use(head.headerConfig);

// Endpoint configuration
restify.serve(router, userModel.UserModel, {
    name: 'users',
});

app.use(router);
app.listen(2000, function () {
    console.info('Server is locating on port 2000');
    console.info('to access localhost:2000/api/v1');
});