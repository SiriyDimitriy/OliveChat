import angular from 'angular';
import uiRouter from '../node_modules/angular-ui-router/release/angular-ui-router.min';
import { ROUTER_STATES } from './app.routes';
import { mockupModule } from './mockup/mockup.module';
import { coreModule } from './core/core.module';
import { examplesModule } from './examples/examples.module';
import { mainFeature2Module } from './main-feature2/main-feature2.module';
import './main.scss';

const app = angular.module('app', [
                                    uiRouter,
                                    mockupModule.name,
                                    coreModule,
                                    examplesModule.name,
                                    mainFeature2Module,
                                  ]);

app.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider.state(ROUTER_STATES.mainState);
    $urlRouterProvider.otherwise('/');
});

export { app };