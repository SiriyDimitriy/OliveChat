class AbstractRepository {
	http;
	config;
	Model;

	constructor($http, config, Model) {
		'NgInject'
		this.http = $http;
		this.config = config;
		this.Model = Model;
	}

	getAll() {
		const url = this.config.url;
		return this.http.get(url)
			.then(res => res.data);
	}

	getItem(itemId) {
		const url = this.config.url + itemId;
		return this.http.get(url)
				   .then(res => res.data);
	}

	createItem(item) {
		const url = this.config.url;
		return this.http.post(url, item)
				   .then(res => res.data);
	}

	updateItem(itemId, data) {
		const url = this.config.url + itemId;
		return this.http.put(url, data)
				   .then(res => res.data);
	}

	deleteItem(itemId) {
		const url = this.config.url + itemId;
		return this.http.delete(url);
		//TODO: Uncoment when fix big with backend http delete response
					// .then(res => res.data);
	}
}

export {AbstractRepository}