/**
 * Message model
 */
export class MessageModel {
	id;
	author;
	messageBody;

	/**
	 * Initializes model
	 */
	constructor(message) {
		this.id = message.id;
		this.author = message.email;
		this.messageBody = message.messageBody;
	}
}