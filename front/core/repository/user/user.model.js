/**
 * User model
 */
export class UserModel {
	id;
	name;
	email;
	password;
	metadata;

	/**
	 * Initializes model
	 */
	constructor(user) {
		this.id = parseInt(user.id, 10) || null;
		this.name = user.name;
		this.email = user.email;
		this.password = user.password;
		this.metadata = user.metadata;
	}
}