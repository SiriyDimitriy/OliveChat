/**
 * Group model
 */
export class GroupModel {
	/**
	 * Initializes model
	 */
	constructor (group) {
		this.id = parseInt(group.id, 10) || null;
		this.name = group.name;
		this.members = group.members;
	}
}