import { GROUP_REPO_CONFIG } from '../repository.config';
import { AbstractRepository } from '../abstract.repository';
import { GroupModel } from './group.model';

class GroupRepository extends AbstractRepository {
	constructor($http){
		'NgInject'
		super($http, GROUP_REPO_CONFIG, GroupModel);

		this.testGroup = {
			id: 6,
			name: 'testGroup',
			members: ['Member1', 'Member2']
		}

		this.updatedGroup = {
			name: 'updateGroup',
			members: ['updateMember1', 'updateMember2']
		}
	}

	getGroup(group) {
		return this.getItem(group.id);
	}

	getAllGroups() {
		return this.getAll();
	}

	createGroup(group) {
		return this.createItem(group);
	}

	updateGroup(group, updatedGroup) {
		return this.updateItem(group.id, updatedGroup);
	}

	deleteGroup(group) {
		return this.deleteItem(group.id);
	}
}

export { GroupRepository };