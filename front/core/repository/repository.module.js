import { UserRepository } from './user/user.repository';
import { MessageRepository } from './message/message.repository';
import { GroupRepository } from './group/group.repository';

const repositoryModule = angular.module('app.core.repository',[])
                                .service('UserRepository', UserRepository)
                                .service('MessageRepository', MessageRepository)
                                .service('GroupRepository', GroupRepository)
                                .name;

export { repositoryModule };