import io from 'socket.io-client';

class SocketService {
	constructor($rootScope) {

		this.getConnection = () => {
			return io('http://localhost:2000/');
		}
		
		this.on = (eventName, callback) => {
			let connection = this.getConnection();

			connection.on(eventName, function() {
				var args = arguments;
				$rootScope.$apply(function() {
					callback.apply(connection, args);
				});
			});
		}

		this.emit = (eventName, data) => {
			let connection = this.getConnection();

			connection.emit(eventName, data, function() {
				var args = arguments;
				$rootScope.$apply(function() {
					if (callback) {
						callback.apply(connection, args);
					}
				});
			});
		}
	}
}

SocketService.$inject = ['$rootScope'];

export { SocketService };