import { UploadService } from './upload-service/upload.service';
import { SocketService } from './socket-service/socket.service';
import ngFileUpload from 'ng-file-upload';


const sharedServicesModule = angular.module('app.core.sharedServices',['ngFileUpload'])
                                    .service('uploadService', UploadService)
                                    .service('SocketService', SocketService)
                                    .name;

export { sharedServicesModule };