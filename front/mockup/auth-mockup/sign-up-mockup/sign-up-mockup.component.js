import signUpMockupTemplate from './sign-up-mockup.component.tpl.html';

class SignUpMockupComponent {
	constructor() {
		this.template = signUpMockupTemplate;
	}
}

export { SignUpMockupComponent };
