import { SignInMockupComponent } from './sign-in-mockup/sign-in-mockup.component';
import { SignUpMockupComponent } from './sign-up-mockup/sign-up-mockup.component';

const AuthMockup = angular.module('app.mockup.AuthMockup', [])
    .component('signInMockup', new SignInMockupComponent())
    .component('signUpMockup', new SignUpMockupComponent())
    .name;

export { AuthMockup };
