import signInMockupTemplate from './sign-in-mockup.component.tpl.html';

class SignInMockupComponent {
	constructor() {
		this.template = signInMockupTemplate;
	}
}

export { SignInMockupComponent };
