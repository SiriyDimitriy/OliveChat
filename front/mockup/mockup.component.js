import mockupTemplate from './mockup.component.tpl.html';

class MockupComponent {
	constructor () {
		this.template = mockupTemplate;
	}
};

export { MockupComponent };