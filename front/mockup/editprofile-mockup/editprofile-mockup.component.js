import editprofileMockupTemplate from './editprofile-mockup.component.tpl.html';

class EditProfileComponent {
    constructor () {
        this.template = editprofileMockupTemplate;
    }
}

export { EditProfileComponent };
