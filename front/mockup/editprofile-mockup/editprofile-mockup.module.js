import { EditProfileComponent } from './editprofile-mockup.component';
import { uploadPhoto } from './upload-photo/upload-photo.component';// import ngFileUpload from 'ng-file-upload';

const EditprofileMockup = angular.module('app.mockup.editprofileMockup', [])
    .component('editprofileMockup', new EditProfileComponent())
    .component('uploadPhoto', new uploadPhoto())
    .name;

export { EditprofileMockup };
 