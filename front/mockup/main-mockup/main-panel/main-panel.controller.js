import { MessageRepository } from '../../../core/repository/message/message.repository';
import { SocketService } from '../../../core/shared-services/socket-service/socket.service';
import io from 'socket.io-client';

class mainPanelController {
	constructor(SocketService, MessageRepository) {
		this.socketService = SocketService;
		this.repository = MessageRepository;
		this.allMessages = [];

		/**
		 * Mock for new Message
		 */
		this.messageData = {
			id: 1,
			author: 'Socket',
			messageBody: '',
		}

		this.getAllMessages();

		this.socketService.on('send:message', (data) => {
			this.allMessages.push(data);
			this.markIterativeMessagesBySameUser(this.allMessages);
		});

	}
	markIterativeMessagesBySameUser(data) {
		for (var i = 0; i < data.length - 1; i++) {
			if (data[i]['author'] === data[i + 1]['author']) {
				data[i + 1]['iterative'] = true;
			} else {
				data[i + 1]['iterative'] = false;
			}
		}
	}
	getAllMessages() {
		this.repository.getAllMessages()
			.then(messagesList => {
				this.allMessages = messagesList;
				this.markIterativeMessagesBySameUser(this.allMessages);
			});
	}

	getMessage(messageId) {
		this.repository.getMessage(messageId)
			.then(message => {
				this.message = message;
			});
	}

	createMessage(messageData) {
		this.repository.createMessage(messageData);
	}

	sendMessage($event, keyCode, messageData) {
		if ($event.keyCode === keyCode) {
			let newMessageData = {};
			Object.assign(newMessageData, messageData);
			this.createMessage(messageData);
			this.socketService.emit('send:message', messageData);

			$event.target.value = '';
		}
	}
}

mainPanelController.$inject = ['SocketService', 'MessageRepository'];

export { mainPanelController };
