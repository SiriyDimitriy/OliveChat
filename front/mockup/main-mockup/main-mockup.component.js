import mainMockupTeplate from './main-mockup.component.tpl.html';
import { MainMockupController } from './main-mockup.controller';

class MainMockupComponent {
	constructor() {
		this.template = mainMockupTeplate;
		this.controller = MainMockupController;
	}
}

export { MainMockupComponent };