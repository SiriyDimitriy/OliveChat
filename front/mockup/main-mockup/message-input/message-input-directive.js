function messageInputDirective() {
    return {
        restrict: 'A',
        link: function($scope, element) {
            $scope.initialHeight = $scope.initialHeight || element[0].style.height;
            var resize = function() {
                element[0].style.height = $scope.initialHeight;
                element[0].style.height = element[0].scrollHeight + "px";
            };
            resize(); //after DOM loading
            element.on("input change", resize);
        }
    };
}

export { messageInputDirective };