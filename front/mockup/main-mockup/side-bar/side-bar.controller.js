import { GroupRepository } from './../../../core/repository/group/group.repository';

class SideBarController {
	constructor (GroupRepository) {
		this.repository = GroupRepository;
		this.groupList = [];
		this.userList = [];
	}

	$onInit () {
		this.repository.getAllGroups()
					   .then(res => this.groupList = res);
		this.repository.getGroup({id: 0})
					   .then(res => this.userList = res.members);
	}
}

export { SideBarController };