import { MainMockupComponent } from './main-mockup.component';
import { MessageContainerComponent } from './message-container/message-container.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { MainPanelComponent } from './main-panel/main-panel.component';
import { messageInputDirective } from './message-input/message-input-directive';
import { scrollBottom } from './scroll-message-container/scrollMessageContainer.directive';

const MainMockup = angular.module('app.mockup.mainMockup', [])
    .component('mainMockup', new MainMockupComponent)
    .component('sideBar', new SideBarComponent)
    .component('messageContainer', new MessageContainerComponent)
    .component('mainPanel', new MainPanelComponent)
    .directive('elastic', messageInputDirective)
    .directive('scrollBottom', scrollBottom)
    .name;

export { MainMockup };