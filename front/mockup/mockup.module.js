import uiRouter from '../../node_modules/angular-ui-router/release/angular-ui-router.min';
import { MOCKUP_ROUTES } from './mockup.routes';

import { MainMockup } from './main-mockup/main-mockup.module';
import { AuthMockup } from './auth-mockup/auth-mockup.module';
import { EditprofileMockup } from './editprofile-mockup/editprofile-mockup.module';
import { MockupComponent } from './mockup.component';
import { ProfilePopover } from './profile-popover/profile-popover.component';

const mockupModule = angular.module('app.mockup', [uiRouter, MainMockup, AuthMockup, EditprofileMockup])
							.component('mockup', new MockupComponent)
							.component('profilePopover', new ProfilePopover);

mockupModule.config($stateProvider => {
	$stateProvider.state(MOCKUP_ROUTES.mockupState)
	              .state(MOCKUP_ROUTES.mainMockupState)
	              .state(MOCKUP_ROUTES.signInMockupState)
	              .state(MOCKUP_ROUTES.signUpMockupState)
                  .state(MOCKUP_ROUTES.editprofileMockupState)
				  .state(MOCKUP_ROUTES.profilePopoverState);
})

export { mockupModule };