export const MOCKUP_ROUTES = {
	mockupState: {
		name: 'mockup',
		url: '/mockup',
		component: 'mockup',
	},

	mainMockupState: {
		name: 'mainMockup',
		url: '/mockup/main-mockup',
		component: 'mainMockup',
	},

	signInMockupState: {
		name: 'signInMockup',
		url: '/mockup/auth-mockup/sign-in-mockup',
		component: 'signInMockup',
	},

	signUpMockupState: {
		name: 'signUpMockup',
		url: '/mockup/auth-mockup/sign-up-mockup',
		component: 'signUpMockup',
	},

	editprofileMockupState: {
		name: 'editprofileMockup',
		url: '/mockup/editprofile-mockup',
		component: 'editprofileMockup',
	},

	profilePopoverState: {
		name: 'profile-popover',
		url: '/mockup/profile-popover',
		component: 'profilePopover',
	}
}
