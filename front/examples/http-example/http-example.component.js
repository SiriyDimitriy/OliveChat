import { HttpExampleController } from './http-example.controller';
import HttpExampleTemplate from './http-example.component.tpl.html';

class HttpExampleComponent {
	constructor() {
		this.template = HttpExampleTemplate;
		this.controller = HttpExampleController;
		this.controllerAs = 'ctrl';
		this.bindings = {
				"hi": "=",
		};
	}
};

export { HttpExampleComponent };