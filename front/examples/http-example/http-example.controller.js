import { UserRepository } from '../../core/repository/user/user.repository';
import * as _ from 'lodash';

class HttpExampleController {
	constructor(UserRepository) {
		this.repository = UserRepository;
		this.allUsers = [];
		this.user = {};

		this.newUser = {
							id: 4,
							name: "newName",
							email: "new@email.com",
							password: "newUserPassword",		
					   };

		this.updatedUser = {
							id: 12,
							name: "updatedUserName",
						  	email: "updated@email.com",
						  };

		this.getAllUsers();
	}

	getAllUsers() {
		this.repository.getAllUsers()
			.then(userList => {
				this.allUsers = userList;
			});
	}

	getUser(userId) {
		this.repository.getUser(userId)
						.then(user => {
							this.user = user;
						});
	}

	createUser(user) {
		this.repository.createUser(user)
			.then(res => {
				this.allUsers.push(res);
			});
	}

	updateUser(user, data) {
		let index = _.indexOf(this.allUsers, _.find(this.allUsers, findUser => findUser.id === user.id));
		this.repository.updateUser(user.id, data)
			.then(res => {
				this.allUsers[index] = res;
			})
	}

	deleteUser(user) {
		let index = _.indexOf(this.allUsers, _.find(this.allUsers, findUser => findUser.id === user.id));
		this.repository.deleteItem(user.id)
			.then(res => _.pull(this.allUsers, res));
	}

	testFunc() {
		this.allUsers.shift();
	}
}

export { HttpExampleController };