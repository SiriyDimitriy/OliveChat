export const EXAMPLES_ROUTES = {
    httpExampleState: {
        name: 'httpExample',
        url: '/http-example',
        component: 'httpExampleComponent',
    },
}