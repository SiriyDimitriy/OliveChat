import uiRouter from '../../node_modules/angular-ui-router/release/angular-ui-router.min';
import { EXAMPLES_ROUTES } from './examples.routes';
import { HttpExampleComponent } from './http-example/http-example.component';

const examplesModule = angular
					 .module('app.examples', [uiRouter,])
					 .component('httpExampleComponent', new HttpExampleComponent);

examplesModule.config(($stateProvider) => {
    $stateProvider.state(EXAMPLES_ROUTES.httpExampleState);
});

export { examplesModule };